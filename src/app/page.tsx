import cn from "clsx";
import Image from "next/image";
import s from "./page.module.css";
import { logotype } from "./logoptype";
import srcUiExample from "./ui-example.png";
import srcCeoPhoto from "./ceo-photo.png";
import srcRocket from "./emoji-rocket.png";
import srcEarth from "./emoji-earth.png";
import srcMemo from "./emoji-memo.png";
import srcMic from "./emoji-mic.png";


export default function Home() {
  return (
    <main
      className={cn(
        s.container,
        "flex flex-col items-center px-12 min-h-screen scroll-smooth"
      )}
    >
      <header className="flex h-[96px] w-full justify-between max-w-[1280px] items-center">
        <p className="p-0">{logotype}</p>

        <ul className="flex items-center gap-[36px] text-sm">
          <li>
            <a href="#" className="text-[#cac6cb]">
              About
            </a>
          </li>
          <li>
            <a href="#features" className="text-[#cac6cb]">
              Features
            </a>
          </li>
          <li>
            <a href="#survey" className="text-[#cac6cb]">
              Survey
            </a>
          </li>
        </ul>
        {/* <p className="p-0">
          <a
            className="px-8 py-4 rounded-lg bg-white text-[#1d1d1d] font-semibold text-sm"
            href="#"
          >
            Take the survey!
          </a>
        </p> */}
      </header>
      <div className="flex items-center flex-col py-20">
        <h1 className="text-7xl leading-[1.20] font-bold whitespace-pre-wrap text-center text-[#cac6cb]">
          Video <span className={cn(s.translation)}>Translation</span>
          {"\n"}
          Made Simple
        </h1>

        <p className="mt-6 max-w-[600px] text-center text-[#cac6cb]">
          We created a service, which will help you{" "}
          <span className="text-[#5e8ee6]">Translate</span> and{" "}
          <span className="text-[#5e8ee6]">Localize</span> your video content
          for international fans. <span className="text-[#5e8ee6]">Easy.</span>
        </p>

        <div className={cn(s.surveyContainer, "p-[1px] mt-36 rounded-xl")}>
          <div className="flex items-center gap-[16px] text-sm bg-[#191520] w-[480px] p-2 rounded-xl justify-between">
            <span className="ml-4 text-[#cac6cb]">Are you interested?</span>

            <a
              href="#survey"
              className={cn(
                s.survey,
                "px-8 py-4 rounded-xl text-base text-white"
              )}
            >
              Let us know
            </a>
          </div>
        </div>
      </div>

      <section
        id="features"
        className="flex flex-wrap justify-between max-w-[642px] w-full my-[120px]"
      >
        <div
          className={cn(s.feature, "relative z-0 md:w-1/2 md:max-w-[300px]")}
        >
          <div className="bg-[#0f0a12] w-full rounded-xl absolute top-[-8px] right-[-8px] h-full -z-10" />
          <div className="bg-[#18101d] min-h-[325px] rounded-xl px-8 py-10 h-full">
            <Image src={srcRocket} alt="ui example" width={72} />
            <h3 className="font-semibold mb-6 mt-8 text-[#cac6cb]">
              Seamless Instant Video Translation
            </h3>
            <p className="text-[#cac6cb]">
              Experience lightning-fast translation for your videos. Expand your
              global reach with our quick and accurate subtitling and dubbing
              feature.
            </p>
          </div>
        </div>
        <div
          className={cn(s.feature, "relative z-0 md:w-1/2 md:max-w-[300px]")}
        >
          <div className="bg-[#0f0a12] w-full rounded-xl absolute top-[-8px] right-[-8px] h-full -z-10" />
          <div className="bg-[#18101d] min-h-[325px] rounded-xl px-8 py-10 h-full">
            <Image src={srcEarth} alt="ui example" width={72} />
            <h3 className="font-semibold mb-6 mt-8 text-[#cac6cb]">
              Amplify Audience
            </h3>

            <p className="text-[#cac6cb]">
              Break language barriers effortlessly, connect with viewers in new
              markets, languages, and demographics. Unlock the true potential of
              your content.
            </p>
          </div>
        </div>
        <div
          className={cn(s.feature, "relative z-0 md:w-1/2 md:max-w-[300px]")}
        >
          <div className="bg-[#0f0a12] w-full rounded-xl absolute top-[-8px] right-[-8px] h-full -z-10" />
          <div className="bg-[#18101d] min-h-[325px] rounded-xl px-8 py-10 h-full">
            <Image src={srcMemo} alt="ui example" width={72} />

            <h3 className="font-semibold mb-6 mt-8 text-[#cac6cb]">
              Fine-tune Your Translations
            </h3>
            <p className="text-[#cac6cb]">
              Edit, refine, and perfect your translated content with our
              intuitive editor. Elevate your content to new heights with
              powerful editing capabilities.
            </p>
          </div>
        </div>
        <div
          className={cn(s.feature, "relative z-0 md:w-1/2 md:max-w-[300px]")}
        >
          <div className="bg-[#0f0a12] w-full rounded-xl absolute top-[-8px] right-[-8px] h-full -z-10" />
          <div className="bg-[#18101d] min-h-[325px] rounded-xl px-8 py-10 h-full">
            <Image src={srcMic} alt="ui example" width={72} />
            <h3 className="font-semibold mb-6 mt-8 text-[#cac6cb]">
              Unleash Your Vocal Doppelgänger
            </h3>
            <p className="text-[#cac6cb]">
              Embrace the power of VoiceClone and emulate your voice or express
              yourself uniquely by getting another sounding for each of selected
              languages. Your choice.
            </p>
          </div>
        </div>
      </section>
      <section
        className={cn(
          s.quote,
          "flex items-center flex-col mt-16 px-16 pt-8 pb-11 max-w-[760px] w-full rounded-[28px] text-[#cac6cb] text-center relative bg-white bg-opacity-10"
        )}
      >
        <span
          className={cn(s.avatar, "relative z-10")}
          style={{ backgroundImage: `url(${srcCeoPhoto.src})` }}
        />
        <p className="whitespace-pre-wrap relative z-10 mt-2 text-[#cac6cb]">
          Sophia Reynolds, CEO
        </p>

        <p className="text-xl whitespace-pre-wrap italic mt-8 relative z-10 text-[#cac6cb]">
          &ldquo;Let your content{" "}
          <span className={s.coloredText}>transcend</span> boundaries,{" "}
          <span className={s.coloredText}>connect</span> with a global audience,
          and make an <span className={s.coloredText}>impact</span> around the
          world&rdquo;
        </p>
      </section>
      <section id="survey" className="flex flex-col items-center">
        <div className="my-16 flex items-center flex-col">
          <p className="text-sm my-9 text-[#cac6cb] text-center max-w-[400px] opacity-50">
            We are aiming to provide the most relevant features, but we
            can&apos;t do it without your help.
          </p>

          <a
            href="https://forms.gle/xMVwLxsFHQi6gJyU7"
            className={cn(
              s.survey,
              "px-8 py-4 rounded-xl text-base text-white"
            )}
          >
            Take the survey
          </a>

          <p className="text-sm mt-9 text-[#cac6cb] text-center max-w-[400px] opacity-50 whitespace-pre-wrap">
            Yep, it&apos;s Google Forms. Please, provide us with your expertise
            all the same. {"\n"}
            Thank you!
          </p>
        </div>

        <div className={s.uiExampleContainer}>
          <div
            className={cn(
              s.fragmentContainer,
              "p-[1px] rounded-xl overflow-hidden items-center max-w-[1024px] h-[500px]"
            )}
          >
            <Image src={srcUiExample} alt="ui example" className="rounded-xl" />
          </div>
        </div>
      </section>
    </main>
  );
}

// Мы создали сервис, который позволит тебе переводить свои видосы так,
// как будто ты озвучил их на другом языке
